# Jahe React Starter
A starter project for React, Redux, Webpack 2, SASS, i18n, autoprefixer, ESLint, React Storybook

# Getting Started
1. Install yarn: https://yarnpkg.com/lang/en/docs/install
2. Install dependencies with `yarn install`
3. Start app with `yarn start`

# React Storybook
Start Storybook
```
yarn run storybook
```

Build a static Storybook
```
build-storybook
```
