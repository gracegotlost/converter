import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../../reducers/uploadState';

import Button from '../../shared/button/button.js';
import Title from '../../shared/title/title.js';

class FileNotPages extends React.Component {

	constructor(props) {
		super(props);
		// console.log('FileNotPages::props: ', props);

		this.onBackToFileVerify = this.onBackToFileVerify.bind(this);
	}

	onBackToFileVerify() {
		this.props.fileVerify();
	}

	render() {
		return (
			<div className='file-not-pages__wrapper'>
				<Title text='好像不是Pages哦!'
						type='primary' />
				<div className='file-not-pages__gif'>
				</div>
				<Button text='再换一个' 
						onClick={ this.onBackToFileVerify } />
			</div>
			);
	}
}

FileNotPages.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(FileNotPages);