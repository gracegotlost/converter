import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../../reducers/uploadState';

import Button from '../../shared/button/button.js';
import Title from '../../shared/title/title.js';

class FileConvertDone extends React.Component {

	constructor(props) {
		super(props);
		// console.log('FileConvertDone::props: ', props);          
		
		this.onDownloadFile = this.onDownloadFile.bind(this);
		this.onBackToFileVerify = this.onBackToFileVerify.bind(this);
	}

	onDownloadFile() {
		// download file
	}

	onBackToFileVerify() {
		this.props.fileVerify();
	}

	render() {
		return (
			<div className='file-convert-done__wrapper'>
				<Title text='转换成功啦！'
						type='primary' />
				<div className='file-convert-done__gif'>
				</div>
				<div className='file-convert-done__btns'>
					<Button text='下载文件' 
						onClick={ this.onDownloadFile } />
					<Button text='继续转换' 
						onClick={ this.onBackToFileVerify } />
				</div>
			</div>
			);
	}
}

FileConvertDone.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(FileConvertDone);