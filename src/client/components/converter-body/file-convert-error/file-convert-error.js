import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../../reducers/uploadState';

import Button from '../../shared/button/button.js';
import Title from '../../shared/title/title.js';

class FileConvertError extends React.Component {

	constructor(props) {
		super(props);
		// console.log('FileConvertError::props: ', props);
		
		this.onBackToFileVerify = this.onBackToFileVerify.bind(this);
	}

	onBackToFileVerify() {
		this.props.fileVerify();
	}

	render() {
		return (
			<div className='file-convert-error__wrapper'>
				<Title text='转换失败，等会再试一下哦！'
						type='primary' />
				<div className='file-convert-error__gif'>
				</div>
				<Button text='重新试一下' 
						onClick={ this.onBackToFileVerify } />        
			</div>
			);
	}
}

FileConvertError.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(FileConvertError);