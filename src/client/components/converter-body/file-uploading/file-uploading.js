import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../../reducers/uploadState';

import Button from '../../shared/button/button.js';
import Title from '../../shared/title/title.js';

class FileUploading extends React.Component {

	constructor(props) {
		super(props);
		// console.log('FileUploading::props: ', props);
		
		this.onUploadingFail = this.onUploadingFail.bind(this);
		this.onUploadingSuccess = this.onUploadingSuccess.bind(this);
	}

	onUploadingFail() {
		this.props.fileUploadError();
	}

	onUploadingSuccess() {
		this.props.fileConverting();
		// API call to covert file
	}

	render() {
		return (
			<div className='file-uploading__wrapper'>
				<Title text='上传中...'
						type='primary' />
				<div className='file-uploading__gif'>
				</div>
				<Button text='上传失败' 
						onClick={ this.onUploadingFail } />
				<Button text='上传成功' 
						onClick={ this.onUploadingSuccess } />
			</div>
			);
	}
}

FileUploading.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(FileUploading);