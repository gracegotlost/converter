import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../../reducers/uploadState';

import Button from '../../shared/button/button.js';
import Title from '../../shared/title/title.js';

class FileUploadError extends React.Component {

	constructor(props) {
		super(props);
		// console.log('FileUploadError::props: ', props);
		
		this.onBackToFileVerify = this.onBackToFileVerify.bind(this);
	}

	onBackToFileVerify() {
		this.props.fileVerify();
	}

	render() {
		return (
			<div className='file-upload-error__wrapper'>
				<Title text='上传失败，等会再试一下哦！'
						type='primary' />
				<div className='file-upload-error__gif'>
				</div>
				<Button text='重新试一下' 
						onClick={ this.onBackToFileVerify } />        
			</div>
			);
	}
}

FileUploadError.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(FileUploadError);