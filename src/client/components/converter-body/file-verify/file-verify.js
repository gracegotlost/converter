import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../../reducers/uploadState';

import Button from '../../shared/button/button.js';
import Title from '../../shared/title/title.js';

class FileVerify extends React.Component {

	constructor(props) {
		super(props);
		// console.log('FileVerify::props: ', props);
		
		this.onOpenFileDialog = this.onOpenFileDialog.bind(this);
		this.onDetectFileType = this.onDetectFileType.bind(this);
		this.updateOverlay = this.updateOverlay.bind(this);

		this.isDraggable = this.isAdvancedUpload();
		this.state = {
			title: this.isDraggable ? '把文件拽进来～' : '打开文件',
			buttonText: this.isDraggable ? '打开也行' : '浏览',
			isDraggingOver: false,
		}
	}

	onOpenFileDialog() {
		this.fileInput.click();
	}

	onDetectFileType() {
		this.fileTypeVerify(this.fileInput.files[0]);
	}

	getDroppedFile(e) {
		this.updateOverlay(e, false);
		this.fileTypeVerify(e.dataTransfer.files[0]);
	}

	fileTypeVerify(file) {
		let fileNameArray = file.name.split('.');
		let fileType = fileNameArray[fileNameArray.length - 1];
		
		if (fileType !== 'pages') {
			this.props.fileNotPages();
		} else {
			this.props.fileFormat(file);
		}
	}

	updateOverlay(e, status) {
		e.preventDefault();
	    e.stopPropagation();
		this.setState({
			isDraggingOver: status
		});
	}

	isAdvancedUpload() {
	    let div = document.createElement('div');
	    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FileReader' in window;
	}

	addWrapperClass() {
		return 'file-verify__wrapper '+ (this.isDraggable ? 'file-verify__wrapper_draggable ' : '') + (this.state.isDraggingOver ? 'file-verify__wrapper_overlay' : '');
	}

	render() {
		return (
			<div className={ this.addWrapperClass() }
				 onDragOver={ e => this.updateOverlay(e, true) }
				 onDragLeave={ e => this.updateOverlay(e, false) }
				 onDrop={ e => this.getDroppedFile(e) }>
				<input type='file' style={{display: 'none'}} 
					   ref={(input) => { this.fileInput = input; }} 
					   onChange={ this.onDetectFileType } />
				<Title text={this.state.title}
						type='primary' />
				<Button text={this.state.buttonText}
						onClick={ this.onOpenFileDialog } />
			</div>
			);
	}
}

FileVerify.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(FileVerify);