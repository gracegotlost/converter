import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../../reducers/uploadState';

import Button from '../../shared/button/button.js';
import Title from '../../shared/title/title.js';

class FileFormat extends React.Component {

	constructor(props) {
		super(props);
		// console.log('FileFormat::props: ', props);

		const {file} = props;

		this.state = {
			file
		}
		
		this.onConvertToDocx = this.onConvertToDocx.bind(this);
		this.onConvertToDoc = this.onConvertToDoc.bind(this);
	}

	onConvertToDocx() {
		console.log('convert to docx');
		this.props.fileUploading();
		// API call to upload file
	}

	onConvertToDoc() {
		console.log('convert to doc');
		this.props.fileUploading();
		// API call to upload file
	}

	render() {
		return (
			<div className='file-format__wrapper'>
				<Title text='转换成什么格式呢？'
						type='primary' />
				<div className='file-format__fileName'>
					{this.state.file.name}
				</div>
				<div className='file-format__btns'>
					<Button text='.docx' 
						onClick={ this.onConvertToDocx } />
					<Button text='.doc' 
						onClick={ this.onConvertToDoc } />
				</div>
			</div>
			);
	}
}

FileFormat.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(FileFormat);