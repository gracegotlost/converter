import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../reducers/uploadState';

import { FILE_VERIFY, FILE_NOT_PAGES, FILE_FORMAT, FILE_UPLOADING, FILE_UPLOAD_ERROR, FILE_CONVERTING, FILE_CONVERT_ERROR, FILE_CONVERT_DONE } from '../../reducers/uploadState';

import FileVerify from './file-verify/file-verify.js';
import FileNotPages from './file-not-pages/file-not-pages.js';
import FileFormat from './file-format/file-format.js';
import FileUploading from './file-uploading/file-uploading.js';
import FileUploadError from './file-upload-error/file-upload-error.js';
import FileConverting from './file-converting/file-converting.js';
import FileConvertError from './file-convert-error/file-convert-error.js';
import FileConvertDone from './file-convert-done/file-convert-done.js';

class ConverterBody extends React.Component {
	constructor(props) {
	    super(props);
	    // console.log('ConverterBody::props: ', props);
	    const { uploadState } = props;

	    this.state = {
	    	uploadState: uploadState.uploadState
	    }
	}

	componentWillReceiveProps(newProps) {
		console.log('ConverterBody::componentWillReceiveProps(): ', newProps);
		const { uploadState } = newProps;

	    this.setState({
	    	uploadState: uploadState.uploadState,
	    	data: uploadState.data ? uploadState.data : null,
	    });

	}

	render() {
		return (
			<div className='converter-body__wrapper'>
				{this.state.uploadState === FILE_VERIFY ? 
					<FileVerify />
	            :null}
	            {this.state.uploadState === FILE_NOT_PAGES ? 
					<FileNotPages />
	            :null}
	            {this.state.uploadState === FILE_FORMAT ? 
					<FileFormat file={this.state.data} />
	            :null}
	            {this.state.uploadState === FILE_UPLOADING ? 
					<FileUploading />
	            :null}
	            {this.state.uploadState === FILE_UPLOAD_ERROR ? 
					<FileUploadError />
	            :null}
	            {this.state.uploadState === FILE_CONVERTING ? 
					<FileConverting />
	            :null}
	            {this.state.uploadState === FILE_CONVERT_ERROR ? 
					<FileConvertError />
	            :null}
	            {this.state.uploadState === FILE_CONVERT_DONE ? 
					<FileConvertDone />
	            :null}
			</div>
			);
	}
}

ConverterBody.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(ConverterBody);