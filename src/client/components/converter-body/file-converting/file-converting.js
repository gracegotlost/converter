import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UploadStateActions from '../../../reducers/uploadState';

import Button from '../../shared/button/button.js';
import Title from '../../shared/title/title.js';

class FileConverting extends React.Component {

	constructor(props) {
		super(props);
		// console.log('FileConverting::props: ', props);
		
		this.onConvertFail = this.onConvertFail.bind(this);
		this.onConvertSuccess = this.onConvertSuccess.bind(this);
	}

	onConvertFail() {
		this.props.fileConvertError();
	}

	onConvertSuccess() {
		this.props.fileConvertDone();
		// API call to covert file
	}

	render() {
		return (
			<div className='file-converting__wrapper'>
				<Title text='转换中...'
						type='primary' />
				<div className='file-converting__gif'>
				</div>
				<Button text='转换失败'
						onClick={ this.onConvertFail } />
				<Button text='转换成功'
						onClick={ this.onConvertSuccess } />
			</div>
			);
	}
}

FileConverting.propTypes = {
};

/* ============================================================
 *
 *       Connect component to Redux
 *
 ============================================================ */

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(UploadStateActions, dispatch);
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapActionCreatorsToProps)(FileConverting);