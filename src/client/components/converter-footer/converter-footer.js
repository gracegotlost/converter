import React from 'react';

const ConverterFooter = () => (
	<div className='converter-footer__wrapper'>
		Made with hunger @2017
	</div>
);

ConverterFooter.propTypes = {
};

export default ConverterFooter;
