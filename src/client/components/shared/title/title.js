import React from 'react';

const Title = ({ type = '', text }) => (
	<div className={'title ' + type}>
		{text}
	</div>
);

Title.propTypes = {
  type: React.PropTypes.string,
  text: React.PropTypes.string
};

export default Title;
