/* ============================================================
 *
 *       Upload Actions
 *
 *
 ============================================================ */

//  Listening states
export const FILE_VERIFY = 'FILE_VERIFY';
export const FILE_NOT_PAGES = 'FILE_NOT_PAGES';
export const FILE_FORMAT = 'FILE_FORMAT';
export const FILE_UPLOADING = 'FILE_UPLOADING';
export const FILE_UPLOAD_ERROR = 'FILE_UPLOAD_ERROR';
export const FILE_CONVERTING = 'FILE_CONVERTING';
export const FILE_CONVERT_ERROR = 'FILE_CONVERT_ERROR';
export const FILE_CONVERT_DONE = 'FILE_CONVERT_DONE';

export function fileVerify() {
  return {
    type: FILE_VERIFY,
  };
}

export function fileNotPages() {
  return {
    type: FILE_NOT_PAGES,
  };
}

export function fileFormat(file) {
  return {
    type: FILE_FORMAT,
    data: file,
  };
}

export function fileUploading() {
  return {
    type: FILE_UPLOADING,
  };
}

export function fileUploadError() {
  return {
    type: FILE_UPLOAD_ERROR,
  };
}

export function fileConverting() {
  return {
    type: FILE_CONVERTING,
  };
}

export function fileConvertError() {
  return {
    type: FILE_CONVERT_ERROR,
  };
}

export function fileConvertDone() {
  return {
    type: FILE_CONVERT_DONE,
  };
}

/* ============================================================
 *
 *       Upload State
 *
 ============================================================ */

 const initialUploadState = {
 	uploadState: FILE_VERIFY
};

const uploadState = (state = initialUploadState, action) => {
	let newState;

	switch (action.type) {
		case FILE_VERIFY:
			newState = { ...state, uploadState: FILE_VERIFY };
			console.log('store: FILE_VERIFY', newState);
			return newState;

		case FILE_NOT_PAGES:
			newState = { ...state, uploadState: FILE_NOT_PAGES };
			console.log('store: FILE_NOT_PAGES', newState);
			return newState;

		case FILE_FORMAT:
			newState = { ...state, uploadState: FILE_FORMAT, data: action.data };
			console.log('store: FILE_FORMAT', newState);
			return newState;

		case FILE_UPLOADING:
			newState = { ...state, uploadState: FILE_UPLOADING };
			console.log('store: FILE_UPLOADING', newState);
			return newState;

		case FILE_UPLOAD_ERROR:
			newState = { ...state, uploadState: FILE_UPLOAD_ERROR };
			console.log('store: FILE_UPLOAD_ERROR', newState);
			return newState;

		case FILE_CONVERTING:
			newState = { ...state, uploadState: FILE_CONVERTING };
			console.log('store: FILE_CONVERTING', newState);
			return newState;

		case FILE_CONVERT_ERROR:
			newState = { ...state, uploadState: FILE_CONVERT_ERROR };
			console.log('store: FILE_CONVERT_ERROR', newState);
			return newState;

		case FILE_CONVERT_DONE:
			newState = { ...state, uploadState: FILE_CONVERT_DONE };
			console.log('store: FILE_CONVERT_DONE', newState);
			return newState;

		default:
			return state;
	}
};

export default uploadState;
