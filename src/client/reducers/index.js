import { combineReducers } from 'redux';
import uploadState from './uploadState';

export default combineReducers({
	uploadState
})
